import os
from collections import defaultdict

class Protein():
    def __init__(self, protein_lines):
        header=protein_lines[0]
        self.id=header.split('|')[1]
        header_ = header.split('|')[-1]
        self.name=header_[:header_.find("OS=")].rstrip()
        self.os=header_[header_.find("OS=")+3:header_.find("OX=")].rstrip()

        if header_.find("GN=") != -1:
            self.ox = header_[header_.find("OX=")+3:header_.find("GN=")].rstrip()
            self.ox = int(self.ox)
            self.gn = header_[header_.find("GN=")+3:header_.find("PE=")].rstrip()
        else:
            self.ox = header_[header_.find("OX=")+3:header_.find("PE=")].rstrip()
            self.gn = None

        self.pe = int(header_[header_.find("PE=")+3:header_.find("SV=")].rstrip())
        self.sv = int(header_[header_.find("SV=")+3:].rstrip())

        self.aminoacids=''.join(protein_lines[1:])
        self.size=len(self.aminoacids)
        self.aminoacidcounts = defaultdict(int)
        for a in self.aminoacids:
            self.aminoacidcounts[a] += 1

    def __repr__(self):
        return f"{self.name} id: {self.id}"

    def __eq__(self, other):
        return self.size == other.size

    def __ne__(self, other):
        return self.size != other.size

    def __lt__(self, other):
        return self.size < other.size

    def __le__(self, other):
        return self.size <= other.size

    def __gt__(self, other):
        return self.size > other.size

    def __ge__(self, other):
        return self.size >= other.size


def get_protein(protein_lines):
    if len(protein_lines) == 0:
        return None
    else:
        return Protein(protein_lines)

def load_fasta(filepath):
    if filepath.split('.')[-1] != 'fasta':
        raise ValueError

    with open(filepath, 'r') as obj:
        lines = obj.readlines()

    ps = []
    protein_lines = []
    num_proteins = 0
    i = 0
    while i < len(lines):
        line = lines[i].rstrip()
        if line.startswith('>sp|') or line.startswith('>tr|'):
            p = get_protein(protein_lines)

            if p is not None:
                ps.append(p)

            protein_lines = []
            num_proteins += 1
        
        protein_lines.append(line)
        i += 1

    ps.append(get_protein(protein_lines))
    
    return ps

def sort_proteins_pe(proteins):
    return sorted(proteins, key=lambda x:x.pe, reverse=True)

def sort_proteins_aa(proteins, a):
    if len(a) != 1:
        raise ValueError("not an aminoacid")
    return sorted(proteins, key=lambda x:x.aminoacidcounts[a], reverse=True)

def find_protein_with_motif(proteins, motif):
    out = []
    for p in proteins:
        if motif in p.aminoacids:
            out.append(p)
    return out